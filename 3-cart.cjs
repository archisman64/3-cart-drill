const products = [{
    shampoo: {
        price: "$50",
        quantity: 4
    },
    "Hair-oil": {
        price: "$40",
        quantity: 2,
        sealed: true
    },
    comb: {
        price: "$12",
        quantity: 1
    },
    utensils: [
        {
            spoons: { quantity: 2, price: "$8" }
        }, {
            glasses: { quantity: 1, price: "$70", type: "fragile" }
        },{
            cooker: { quantity: 4, price: "$900" }
        }
    ],
    watch: {
        price: "$800",
        quantity: 1,
        type: "fragile"
    }
}]


/*

Q1. Find all the items with price more than $65.
Q2. Find all the items where quantity ordered is more than 1.
Q.3 Get all items which are mentioned as fragile.
Q.4 Find the least and the most expensive item for a single quantity.
Q.5 Group items based on their state of matter at room temperature (Solid, Liquid Gas)

NOTE: Do not change the name of this file

*/ 

function simiplifyData (data) {
    const correctedData = data.map((obj) => {

        // const obj = data[0];
        const keys = Object.keys(obj);
        const itemsWithoutNesting = keys.reduce((acc, currKey) => {
    
            const productInfo = obj[currKey];
    
            if(Array.isArray(productInfo)) {
                const innerItems = productInfo.reduce((innerAcc, innerKey) => {
                    innerAcc = {
                        ...innerAcc,
                        ...innerKey
                    }
                    return innerAcc;
                }, {});
                
                acc = {
                    ...acc,
                    ...innerItems
                }
            } else {
                acc[currKey] = productInfo;
            }
            return acc;
        }, {});
        return itemsWithoutNesting;
    })
    return correctedData[0];
}

//      1       //
function findItemsCostingMoreThan$65 (items) {

    const itemsWithoutNesting = simiplifyData(items);
    
    const itemsCostingMoreThan$65 = Object.entries(itemsWithoutNesting).filter((item) => {
        const [productName, productInfo] = item;
        return Number(productInfo.price.replace('$', '')) > 65;
    })

    return Object.fromEntries(itemsCostingMoreThan$65);
}
console.log('items with price more than $65:', findItemsCostingMoreThan$65(products));
// console.log('products:', products);

//      2       //
function findItemsWithOrderQuantityMoreThan1 (items) {
    const itemsWithoutNesting = simiplifyData(items);

    const itemsWithQuantityMoreThan1 = Object.entries(itemsWithoutNesting).filter(([name, info]) => {
        return info.quantity > 1;
    })

    return Object.fromEntries(itemsWithQuantityMoreThan1);
}
console.log('items with order quantity more than 1:', findItemsWithOrderQuantityMoreThan1(products));

//      3       //
function findFragileItems (items) {
    const itemsWithoutNesting = simiplifyData(items);

    const fragileItems = Object.entries(itemsWithoutNesting).filter(([name, info]) => {
        return (info.type && info.type === 'fragile');
    })
    return Object.fromEntries(fragileItems);
}
console.log('fragile items:', findFragileItems(products));

//      4       //
function findLeastAndMostExpensiveItems (items) {
    const itemsWithoutNesting = simiplifyData(items);

    const itemsSortedByPrice = Object.entries(itemsWithoutNesting).sort((first, second) => {
        const singleQuantityPriceOf1 = +first[1].price.replace('$', '') / first[1].quantity;
        const singleQuantityPriceOf2 = +second[1].price.replace('$', '') / second[1].quantity;

        return singleQuantityPriceOf1 - singleQuantityPriceOf2;
    });
    return Object.fromEntries([itemsSortedByPrice[0], itemsSortedByPrice[itemsSortedByPrice.length - 1]])    
}
console.log('least and most expensive items:', findLeastAndMostExpensiveItems(products));

//      5       //
function groupItemsByStateOfMatter (items) {
    const itemsWithoutNesting = simiplifyData(items);
    const itemNames = Object.keys(itemsWithoutNesting);
    
    const groupedItems = itemNames.reduce((acc, name) => {
        if(name.toLowerCase().includes('shampoo') || name.toLowerCase().includes('oil')) {
            const newObj = {};
            newObj[name] = itemsWithoutNesting[name];
            // console.log(newObj);
            acc['liquid'].push(newObj);
        } else {
            const newObj = {};
            newObj[name] = itemsWithoutNesting[name];
            acc['solid'].push(newObj);
        }
        return acc;
    }, { 'solid': [], 'liquid': []});
    return groupedItems;
}
console.log('items grouped by state of matter:', groupItemsByStateOfMatter(products));
// console.log(products);

// console.log(simiplifyData(products));
